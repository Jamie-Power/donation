package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;


@Entity

public class Donation extends Model
{

	public long recieved;
	public String methodDonated;
	
	@ManyToOne
	public User from;
	
	public Donation(User from, String methodDonated, long recieved)
	{
		this.recieved = recieved;
		this.methodDonated = methodDonated;
		this.from = from;
	}
	
	
}
