package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class DonationController extends Controller
{

	  public static void index()
	  {	  
		  User user = Accounts.getCurrentUser();
		  if (user ==null)
		  {
			  Logger.info("Donation class : Unable to getCurrentUser");
			  Accounts.login();
		  }
		  else
		  {
			 String prog = getPercentTargetAchieved();
			 String progress = prog + "%";
			 Logger.info("Donation ctrler : percent target achieved " + progress);
			 render(user, progress);
		  }
	  }
	  
	  public static void donate( long amountDonated, String methodDonated)
	  {
		  Logger.info("method donated " + amountDonated + " " + "method donated " + methodDonated);
		  
		  User user = Accounts.getCurrentUser();
		  if (user == null)
		  {
			  Logger.info("Donation class : unable to getCurrentUser");
			  Accounts.login();
		  }
		  else
		  {
			  addDonation(user, methodDonated, amountDonated);
		  }
		  index();
	  }
	  
	  private static void addDonation(User user, String methodDonated, long amountDonated)
	  {
		  Donation bal = new Donation(user, methodDonated, amountDonated);
		  bal.save();
	  }

	  private static long getDonationTarget()
	  {
		  return 20000;
	  }
	  
	  public static String getPercentTargetAchieved()
	  {
		  List<Donation> allDonations = Donation.findAll();
		  long total = 0;
		  for(Donation donation : allDonations)
	    {
			  total += donation.recieved;
	    }
		  long target = getDonationTarget();
		  if(total >= target){
			 total = target;
		  }
		  long percentachieved = (total * 100/ target);
		  String progress = String.valueOf(percentachieved);
		  return progress;
		  
      }
	  
}
